package com.assignment.app.commandlinecalc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	Calculator calc;
	@Before
	public void setUp() throws Exception {
		 calc = new Calculator();
	}

	@After
	public void tearDown() throws Exception {
		calc = null;
	}

	

	
	@Test(expected=IllegalArgumentException.class)
	public void testExprEvalThrowsIllegalArgException_unsupportedMethod() {
		calc.exprEval("UNsupported(1,2))");
		
	}

	@Test
	public void testExprEvalAdd() {
	
		assertEquals(3,calc.exprEval("add(1,2)"));
		
	}
	
	@Test
	public void testExprEvalLet() {
		assertEquals(40,calc.exprEval("let(a,let(b,10,add(b,b)), let(b,20,add(a,b)))"));
	}
	
	@Test
	public void testExprEvalMult() {
		String expression = "mult(add(2,2),div(9,3))";
	
		assertEquals(12,calc.exprEval(expression));
	}
	
	

}
