# README #

This is a calculator program in Java that evaluates expressions in a very simple integer expression language. The program takes an input on the command line, computes the result, and prints it to the console.  For example:
% java calculator.Main "add(2, 2)"
4
Few more examples:


IN - add(1, mult(2, 3)), OUT - 7

IN -mult(add(2, 2), div(9, 3)),OUT - 12

IN -let(a, 5, let(b, mult(a, 10), add(b, a))),OUT - 55

IN -let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b))),OUT - 40

An expression is one of the of the following:
	•	Numbers: integers between Integer.MIN_VALUE and Integer.MAX_VALUE
	•	Variables: strings of characters, where each character is one of a-z, A-Z
	•	Arithmetic functions: **add, sub, mult, div**, each taking two arbitrary expressions as arguments.  In other words, each argument may be any of the expressions on this list.
	•	A “let” operator for assigning values to variables:
	let(<variable name>, <value expression>, <expression where variable is used>)
As with arithmetic functions, the value expression and the expression where the variable is used may be an arbitrary expression from this list. 



**### How do I get set up? ###**

1. clone the repository
2. Navigate to local repository after cloning i.e. cd command_line_calc
3. Build thru maven. ie. run "mvn clean install"



**###  Running the app ###**

Since this app has dependencies on log4j etc, please run below command to have the dependencies added in the classpath from root directory ( same level as pom.xml)

$ java -Dlog4j.configuration={LOCAL_LOG4J_CONFIG_FILE}  -cp target/command-line-calc-1.0-SNAPSHOT-jar-with-dependencies.jar com.assignment.app.commandlinecalc.Calculator **"add(1,9)"** 

NOTE: Default logging is set to ERROR. Please use -Dlog4j.configuration option to override the logging level.



**### Dependencies   ###**

log4j

jUnit



### Who do I talk to? ###

* nisharawat85@gmail.com